const HtmlPlugin = require('html-webpack-plugin');
const CssPlugin = require('mini-css-extract-plugin');

const mode = process.env.NODE_ENV !== 'production';

console.log(mode)

module.exports = {
    entry: [
        "./src/app/js/index.js"
    ],
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js'
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    mode ? 'style-loader' : CssPlugin.loader,
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlPlugin({
            template: './src/app/index.html'
        }),
        new CssPlugin({
            filename: 'bundle.css'
        })
    ]
};